import React, { useMemo, useState } from 'react'

const NameTab = ({prevTab}) => {
    return (
        <div>
            <input type="text"/>
            <button onClick={prevTab}>anterior</button>
        </div>
    )
}

const DateTab = ({nextTab}) => {
    return (
        <div>
            <input />
            <h1>asd</h1>
            <button onClick={nextTab}>siguiente</button>
        </div>
    )
}

function NewForm() {
    const [tabNumber, setTabNumber] = useState(0);
    
    const nextTab = () => {
        setTabNumber(number => number + 1);
    }

    const prevTab = () => {
        setTabNumber(number => number - 1);
    }

    const memoTabs = useMemo(() => {
        return {
            0: () => <DateTab nextTab={nextTab}/>,
            1: () => <NameTab prevTab={prevTab}/>
        } 
    }, [])

    const tab = {
        0: () => <DateTab nextTab={nextTab}/>,
        1: () => <NameTab prevTab={prevTab}/>
    }

    return (
        <div>
            {memoTabs[tabNumber]()}
        </div>
    )
}

export default NewForm;
