import React, { useMemo, useState, useContext } from 'react'
////

const FormContext = React.createContext();

const FormProvider = ({children}) => {
    const [tabNumber, setTabNumber] = useState(0);
    
    const nextTab = () => {
        setTabNumber(number => number + 1);
    }

    const prevTab = () => {
        setTabNumber(number => number - 1);
    }

    const memoValues = useMemo(() => {
        return {
            tabNumber,
            nextTab,
            prevTab
        }
    }, [tabNumber])


    return (
        <FormContext.Provider value={memoValues}>
            {children}
        </FormContext.Provider>
    )
}

////////
const NameTab = () => {
    const { prevTab } = useContext(FormContext);

    return (
        <div>
            <input type="text"/>
            <button onClick={prevTab}>anterior</button>
        </div>
    )
}

const DateTab = () => {
    const { nextTab } = useContext(FormContext);

    return (
        <div>
            <input />
            <h1>asd</h1>
            <button onClick={nextTab}>siguiente</button>
        </div>
    )
}

const Display = () => {
    const { tabNumber } = useContext(FormContext);

    const memoTabs = useMemo(() => {
        return {
            0: () => <DateTab />,
            1: () => <NameTab />
        } 
    }, [])

    return (
        memoTabs[tabNumber]()
    )
}

function NewForm() {
    

    return (
        <FormProvider>
            <Display />
        </FormProvider>
    )
}

export default NewForm;
