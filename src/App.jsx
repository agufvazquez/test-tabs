import React, { useState } from 'react'
import logo from './logo.svg'
import './App.css'

import NewForm from './components/NewForm'
import NewFormWithContext from './components/NewFormWithContext';

function App() {
  const [count, setCount] = useState(0)

  return (
    <div>
      <NewFormWithContext />
    </div>
  )
}

export default App
